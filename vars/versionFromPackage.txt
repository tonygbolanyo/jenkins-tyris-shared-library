Find the version number from package.json file in project root.
It adds build number and environment (from NODE_ENV var) and
set this version to VERSION environment variable.