def call() {
  filePath = './package.json'
  if (!fileExists(filePath)) {
    return
  }
  props = readJSON file: filePath
  versionNumber = props['version'] + '.' + env.BUILD_ID + '-' + env.NODE_ENV
  env.VERSION = sh(script: 'echo "' + versionNumber + '" | sed -e "s/\\./-/g"', returnStdout: true)
  env.VERSION = env.VERSION.toLowerCase()
}
