def call(Map config) {
  if (!config.projectId) {
    echo 'CONFIG ERROR: Needs projectId to get credentials for GCP'
    return
  }
  versionFromPackage()
  withCredentials([file(credentialsId: config.projectId, variable: 'GOOGLE_APPLICATION_CREDENTIALS')]) {
    sh("gcloud auth activate-service-account --key-file ${GOOGLE_APPLICATION_CREDENTIALS}")
    sh("gcloud config set project ${config.projectId}")
    sh("gcloud app deploy --version=${VERSION}")
    echo "Deployed to GCP: ${config.projectId}"
  }
}
