def call(Map config) {
  if (!config.projectId) {
    echo "CONFIG ERROR: Needs projectId to send info to JIRA"
    return
  }
  if (!config.envType) {
    echo "CONFIG ERROR: Needs envType to send info to JIRA"
    return
  }
  if (!config.state) {
    echo "CONFIG ERROR: Needs state to send info to JIRA"
    return
  }
  jiraSendDeploymentInfo (
    environmentId: config.projectId,
    environmentName: config.projectId,
    environmentType: config.envType,
    site: 'tyris-sw.atlassian.net',
    state: config.state)
}
