def call() {
  items = [
    '.scannerwork',
    'coverage',
    'cy-cache',
    'dist',
    'node_modules',
    'npm-cache',
    'reports',
    'yarn-cache',
  ]
  items.each {
    item -> sh 'rm -rf ${item}'
  }
}
